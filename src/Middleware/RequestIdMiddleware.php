<?php

namespace Dotcubed\LaravelApiResponse\Middleware;

use Closure;
use Illuminate\Http\Request;

class RequestIdMiddleware
{
    public function handle(Request $request, Closure $next): mixed
    {
        $requestId = $request->header('X-Request-ID') ?? uniqid();
        $request->headers->set('X-Request-ID', $requestId);

        // Add the request ID to the response for tracing in client-side
        $response = $next($request);
        $response->headers->set('X-Request-ID', $requestId);

        return $response;
    }
}
