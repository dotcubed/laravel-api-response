<?php

namespace Dotcubed\LaravelApiResponse;

use Illuminate\Support\ServiceProvider;

class LaravelApiResponseServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $kernel = $this->app->make(\Illuminate\Contracts\Http\Kernel::class);
        $kernel->pushMiddleware(\Dotcubed\LaravelApiResponse\Middleware\RequestIdMiddleware::class);   
    }
}
